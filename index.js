console.log("Hello	World");

let username;
let password;
let role;

// start of login function
function loginFunction(){
	username = prompt("Please enter your username:");
	password = prompt("Please enter your password:");
	role = prompt("Please enter your role:").toLowerCase();

	if((username === "" || username === null) || (password === "" || password === null) || (role === "" || role === null)){
		alert("Invalid inputs! Every field should not be empty.");
	} else {
		switch(role){
			case 'admin':
				alert("Welcome back to the class portal, admin!");
				break;
			case 'teacher':
				alert("Thank you for logging in, teacher!");
				break;
			case 'student':
				alert("Welcome to the class portal, student!");
				break;
			default:
				console.log("Role out of range.");
				break;
		}
	}
}
// end of login function

loginFunction();

//  start of average function
function averageRangeCalculator(score1, score2, score3, score4){
	let average = (score1 + score2 + score3 + score4) / 4;
	average = Math.round(average);
	// console.log(average);

	if(average <= 74){
		console.log("Hello student, your average is " + average + "." + " The letter equivalent is F.");
	} else if(average >= 75 && average <= 79){
		console.log("Hello student, your average is " + average + "." + " The letter equivalent is D.");
	} else if(average >= 80 && average <= 84){
		console.log("Hello student, your average is " + average + "." + " The letter equivalent is c.");
	} else if(average >= 85 && average <= 89){
		console.log("Hello student, your average is " + average + "." + " The letter equivalent is B.");
	} else if(average >= 90 && average <= 95){
		console.log("Hello student, your average is " + average + "." + " The letter equivalent is A.");
	} else if(average >= 96){
		console.log("Hello student, your average is " + average + "." + " The letter equivalent is A+.");
	}
}
// end of average function
averageRangeCalculator(0,0,0,0);